﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using QuizBackend.Models;

namespace QuizConsoleFrontend
{
    public class QuizConsole
    {
        private readonly QuizClient _client;
        private string _user;

        public QuizConsole(string? host)
        {
            _client = new QuizClient(host);
            Console.WriteLine("Welcome!!");
            Login();
            while (true)
            {
                Console.WriteLine("");
                DisplayMenu();
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private void Play()
        {
            string quizType = GetQuizType();

            var points = 0;
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            
            for (var i = 0; i < 5; i++)
            {
                if (AskQuestion(quizType)) points++;
            }

            var duration = stopWatch.Elapsed;
            Console.WriteLine($"Congrats {_user}! You got {points} points and needed {duration.TotalMilliseconds:0.00} ms");
            try
            {
                _client.CreateStatistic(_user, points, duration.TotalMilliseconds).Wait();
            }
            catch
            {
                Console.WriteLine("Couldn't create the stat");
            }
            DisplayTopStats(3);
        }

        private void DisplayMenu()
        {
            var options = new Dictionary<string, Action>()
            {
                ["play"] = Play,
                ["global stats"] = () => DisplayTopStats(int.MaxValue),
                ["my stats"] = () => DisplayUserStats(_user),
                ["change user"] = Login,
                ["exit"] = () => Environment.Exit(2)
            };
            
            Console.WriteLine("Your options: ");
            var opts = options.Keys.ToArray();
            for (var i = 0; i < opts.Length; i++)
            {
                Console.WriteLine($"{i}) {opts[i]}");
            }

            var input = Prompt("> ", opts.Length - 1);
            Console.WriteLine("");
            options[opts[input]].Invoke();
        }

        private void DisplayUserStats(string user)
        {
            try
            {
                var stats = _client.GetStatsByUser(user).Result;
                
                if(stats.Length == 0)
                {
                    Console.WriteLine("You don't have stats yet.");
                    return;
                }
                DisplayStats(stats);
            }
            catch
            {
                Console.WriteLine("Couldn't display the stats");
            }        
        }

        private void DisplayTopStats(int n)
        {
            try
            {
                var stats = _client.GetTopNStats(n).Result;
                DisplayStats(stats);
            }
            catch
            {
                Console.WriteLine("Couldn't display the stats");
            }        
        }

        private void DisplayStats(Statistic[] stats)
        {
            var longestName = stats.Max(s => s.User.Length);
            var longestDuration = stats.Max(s => $"{s.Duration:0.00}".Length);
            Console.WriteLine("Ranking: ");
            foreach (var statistic in stats)
            {
                Console.WriteLine($"{FillWithSpaces(statistic.User, longestName)} - {statistic.Points} points - {FillWithSpaces($"{statistic.Duration:0.00}", longestDuration)} ms - {statistic.Date}");
            }
        }

        private string FillWithSpaces(string value, int requiredLength)
        {
            var str = new StringBuilder(value);
            
            while (str.Length != requiredLength)
            {
                str.Append(' ');
            }

            return str.ToString();
        }

        private void Login()
        {
            Console.Write("Enter your name: ");
            var user = Console.ReadLine();

            if (user is null || user.Trim() == "") user = "anonymous";
            _user = user;
        }

        private string GetQuizType()
        {
            try
            {
                var types = _client.GetQuizTypes().Result;

                Console.WriteLine("Choose the type of quiz you want to play: ");
                for (var i = 0; i < types.Length; i++)
                {
                    Console.WriteLine($"{i}) {types[i]}");
                }
                Console.WriteLine($"{types.Length}) all");
                var input = Prompt("> ", types.Length);

                return input == types.Length ? "" : types[input];
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return "";
            }
        }

        private bool AskQuestion(string type)
        {
            try
            {
                var question = _client.GetQuestion( type ).Result;
                
                Console.WriteLine(question.Question);
                for (var i = 0; i < question.Answers.Length; i++)
                {
                    Console.WriteLine($"{i}: {question.Answers[i].Name}");
                }
                var answerInput = Prompt("> ", 2);
                var correctAnswer = question.Answers[answerInput].Value.Equals(question.Solution);
                Console.WriteLine((correctAnswer ? "Correct." : "Wrong.") + " The values are: ");
                foreach (var answer in question.Answers)
                {
                    Console.WriteLine($"{answer.Name} -> {answer.Value} {question.Unit}");
                }
                
                Console.WriteLine("");
                
                return correctAnswer;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
        
        private static int Prompt(string? prompt, int maxValue)
        {
            var output = -1;
            while (output > maxValue || output < 0)
            {
                var input = "";
                try
                {
                    Console.Write(prompt);
                    input = Console.ReadLine();
                    if (input != null) output = int.Parse(input);
                    if (output > maxValue || output < 0) throw new Exception();
                }
                catch
                {
                    Console.WriteLine($"\"{input}\" is not a valid input");
                }
            }

            return output;
        }
    }
}