﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using QuizBackend.Models;

namespace QuizConsoleFrontend
{
    public class QuizClient
    {
        private readonly string _host;

        public QuizClient(string? host)
        {
            this._host = host ?? "https://localhost:7141/";
        }

        public async Task<QuizQuestion> GetQuestion()
        {
            using var httpClient = new HttpClient();
            var result = await httpClient.GetAsync(_host + "api/quiz/question");

            if (result.IsSuccessStatusCode) throw new Exception("Can't connect to the Backend");

            return JsonSerializer.Deserialize<QuizQuestion>(await result.Content.ReadAsStringAsync())!;
        }

        public async Task<QuizQuestion> GetQuestion(string type)
        {
            using var httpClient = new HttpClient();
            var result = await httpClient.GetAsync(_host + $"api/quiz/question?quizType={type}");

            if (!result.IsSuccessStatusCode) throw new Exception("Can't connect to the Backend");

            return JsonSerializer.Deserialize<QuizQuestion>(await result.Content.ReadAsStringAsync())!;
        }

        public async Task<string[]> GetQuizTypes()
        {
            using var httpClient = new HttpClient();
            var result = await httpClient.GetAsync(_host + "api/quiz/types");

            if (!result.IsSuccessStatusCode)
            {
                throw new Exception("Can't connect to the Backend");
            }
            
            return JsonSerializer.Deserialize<string[]>(await result.Content.ReadAsStringAsync())!;
        }

        public async Task<Statistic[]> GetTopNStats(int n)
        {
            using var httpClient = new HttpClient();
            var result = await httpClient.GetAsync(_host + $"api/stats/top/{n}");

            if (!result.IsSuccessStatusCode)
            {
                throw new Exception("Can't connect to the Backend");
            }
            
            return JsonSerializer.Deserialize<Statistic[]>(await result.Content.ReadAsStringAsync())!;
        } 
        
        public async Task<Statistic[]> GetStatsByUser(string user)
        {
            using var httpClient = new HttpClient();
            var result = await httpClient.GetAsync(_host + $"api/stats/user?user={user}");

            if (!result.IsSuccessStatusCode)
            {
                return Array.Empty<Statistic>();
            }
            
            return JsonSerializer.Deserialize<Statistic[]>(await result.Content.ReadAsStringAsync())!;
        } 
        
        public async Task CreateStatistic(string user, int points, double duration)
        {
            using var httpClient = new HttpClient();
            var result = await httpClient.PostAsync(_host + $"api/stats/create?user={user}&points={points}&duration={duration:0.00}", null);

            if (!result.IsSuccessStatusCode)
            {
                throw new Exception("Can't connect to the Backend");
            }
        } 
        
    }
}