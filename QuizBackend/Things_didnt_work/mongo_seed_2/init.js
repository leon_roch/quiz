db = db.getSiblingDB("quiz")

db.createUser({
    user: "app",
    pwd: "s3cr3t!PwD",
    roles: [{
        role: "readWrite",
        db: "quiz"
    }]
});

db.createCollection("objects");
db.createCollection("users");
db.createCollection("statistics");
