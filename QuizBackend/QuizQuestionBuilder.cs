﻿using System;
using System.Collections.Generic;
using System.Linq;
using QuizBackend.Models;

namespace QuizBackend
{
    public class QuizQuestionBuilder
    {
        public QuizQuestion Build(List<QuizObject> objects)
        {
            var obj = objects[0]; // used to access the general information faster

            var rand = new Random();

            // the question uses only 1 attribute
            var attributeIndex = rand.Next(0, obj.Attributes.Length);
            
            var useMin = rand.Next(0, 2) == 0;

            return new QuizQuestion
            {
                Question = BuildQuestion(useMin, attributeIndex, obj),
                Unit = obj.Attributes[attributeIndex].Unit,
                Answers = objects.Select(o => new Answer
                {
                    Name = o.Name,
                    Value = o.Attributes[attributeIndex].Value
                }).ToArray(),
                Solution = GetSolution(useMin, attributeIndex, objects)
            };
            
        }

        private string BuildQuestion(bool useMin,int atrr, QuizObject obj)
        {
            if (useMin)
            {
                return $"Which {obj.QuizType} {obj.Attributes[atrr].MinWinsText}?";
            }
            
            return $"Which {obj.QuizType} {obj.Attributes[atrr].MaxWinsText}?";
        }

        private double GetSolution(bool useMin, int attributeIndex , List<QuizObject> objects)
        {
            return useMin
                ? objects.MinBy(
                        o => o.Attributes[attributeIndex].Value
                    )
                    !.Attributes[attributeIndex].Value
                : objects.MaxBy(
                        o => o.Attributes[attributeIndex].Value
                    )
                    !.Attributes[attributeIndex].Value
                ;


        }
    }
}