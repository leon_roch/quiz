﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace QuizBackend.Models
{
    public class Attribute
    {
        [BsonElement("name")]
        public string Name { get; set; } = null!;

        [BsonElement("value")]
        public double Value { get; set; }
        
        [BsonElement("unit")]
        public string Unit { get; set; } = null!;
        
        [BsonElement("maxDisplay")]
        public string MaxWinsText { get; set; } = null!;
        
        [BsonElement("minDisplay")]
        public string MinWinsText { get; set; } = null!;
    }
}