﻿using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace QuizBackend.Models
{
    public class Statistic
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [JsonPropertyName("id")]
        public string? Id { get; set; }

        [BsonElement("date")] 
        [JsonPropertyName("date")]
        public string Date { get; set; } = null!;
        
        [BsonElement("user")]        
        [JsonPropertyName("user")]
        public string User { get; set; } = null!;
        
        [BsonElement("points")]
        [JsonPropertyName("points")]
        public int Points { get; set; }

        [BsonElement("duration")]
        [JsonPropertyName("duration")]
        public double Duration { get; set; }
    }
}