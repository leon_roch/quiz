﻿using System.Text.Json.Serialization;

namespace QuizBackend.Models
{
    public class QuizQuestion
    {
        [JsonPropertyName("question")]
        public string Question { get; set; } = null!;

        [JsonPropertyName("answers")]
        public Answer[] Answers { get; set; } = null!;

        [JsonPropertyName("unit")]
        public string Unit { get; set; } = null!;

        [JsonPropertyName("solution")]
        public double Solution { get; set; }
    }
}