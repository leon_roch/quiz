﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace QuizBackend.Models
{
    public class QuizObject
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }

        [BsonElement("quiz")]
        public string QuizType { get; set; } = null!;

        [BsonElement("name")]
        public string Name { get; set; } = null!;
        
        [BsonElement("attributes")]
        public Attribute[] Attributes { get; set; } = null!;
    }
}