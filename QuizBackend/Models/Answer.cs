﻿using System;
using System.Text.Json.Serialization;

namespace QuizBackend.Models
{
    public class Answer
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = null!;
        
        [JsonPropertyName("value")]
        public double Value { get; set; }
    }
}