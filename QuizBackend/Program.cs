using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using QuizBackend;
using QuizBackend.Config;
using QuizBackend.Repositories;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Convert appsettings.json to QuizMongoDBOptions Object
builder.Services.Configure<QuizMongoDbOptions>(
    builder.Configuration.GetSection("QuizMongoDB")
    );

// quiz stuff
builder.Services.AddSingleton<QuizRepository>();
builder.Services.AddSingleton<QuizQuestionBuilder>();

// statistics stuff
builder.Services.AddSingleton<StatisticsRepository>();

builder.Services.AddCors(options =>
    options.AddDefaultPolicy(policy =>
        policy
            .AllowAnyOrigin()
            .AllowAnyHeader()
            .AllowAnyMethod()
    ));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.UseCors();

app.Run();