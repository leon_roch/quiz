﻿namespace QuizBackend.Config
{
    public class QuizMongoDbOptions
    {
        public string ConnectionString { get; set; } = null!;

        public string DatabaseName { get; set; } = null!;
        
        public string QuizCollection { get; set; } = null!;
        
        public string UserCollection { get; set; } = null!;
        
        public string StatisticCollection { get; set; } = null!;
    }
}