db = db.getSiblingDB("quiz")

db.createUser({
    user: "app",
    pwd: "s3cr3t!PwD",
    roles: [{
        role: "readWrite",
        db: "quiz"
    }]
});

db.createCollection("objects");
db.createCollection("statistics");


db.objects.insertMany([
    {
        "quiz": "animal",
        "name": "Elephant",
        "attributes": [
            {
                "name": "age",
                "value": 60,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 40,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 3.65,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 6000,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    },
    {
        "quiz": "animal",
        "name": "Lion",
        "attributes": [
            {
                "name": "age",
                "value": 15,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 56,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 2,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 250,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    },
    {
        "quiz": "animal",
        "name": "Giraffe",
        "attributes": [
            {
                "name": "age",
                "value": 25,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 60,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 5.2,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 1200,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    },
    {
        "quiz": "animal",
        "name": "Crocodile",
        "attributes": [
            {
                "name": "age",
                "value": 70,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 27,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 4.9,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 1000,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    },
    {
        "quiz": "animal",
        "name": "Chimpanzee",
        "attributes": [
            {
                "name": "age",
                "value": 40,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 39,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 0.9,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 60,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    },
    {
        "quiz": "animal",
        "name": "Gazelle",
        "attributes": [
            {
                "name": "age",
                "value": 15,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 65,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 1.1,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 30,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    },
    {
        "quiz": "animal",
        "name": "Penguin",
        "attributes": [
            {
                "name": "age",
                "value": 20,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 8,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 1.2,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 20,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    },
    {
        "quiz": "animal",
        "name": "Bear",
        "attributes": [
            {
                "name": "age",
                "value": 30,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 56,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 1.8,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 800,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    },
    {
        "quiz": "animal",
        "name": "Snake",
        "attributes": [
            {
                "name": "age",
                "value": 10,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 21,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 9.0,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 200,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    },
    {
        "quiz": "animal",
        "name": "Eagle",
        "attributes": [
            {
                "name": "age",
                "value": 15,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 300,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 0.99,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 5,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    },
    {
        "quiz": "animal",
        "name": "Kangaroo",
        "attributes": [
            {
                "name": "age",
                "value": 20,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 2,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 1.4,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 50,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    },
    {
        "quiz": "animal",
        "name": "Cougar",
        "attributes": [
            {
                "name": "age",
                "value": 15,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 80,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 1.5,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 70,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    },
    {
        "quiz": "animal",
        "name": "Rhino",
        "attributes": [
            {
                "name": "age",
                "value": 50,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 55,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 3.8,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 2000,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    },
    {
        "quiz": "animal",
        "name": "Tiger",
        "attributes": [
            {
                "name": "age",
                "value": 20,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 65,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 2.92,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 350,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    },
    {
        "quiz": "animal",
        "name": "Capybara",
        "attributes": [
            {
                "name": "age",
                "value": 10,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 35,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 1.2,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 60,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    },
    {
        "quiz": "animal",
        "name": "Cat",
        "attributes": [
            {
                "name": "age",
                "value": 14,
                "unit": "years",
                "maxDisplay": "becomes the oldest",
                "minDisplay": "grows the least old"
            },
            {
                "name": "speed",
                "value": 48,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "height",
                "value": 0.5,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "weight",
                "value": 4,
                "unit": "kilograms",
                "maxDisplay": "is the heaviest",
                "minDisplay": "is the lightest"
            }
        ]
    }
]) // animals

db.objects.insertMany([
    {
        "quiz": "car",
        "name": "Lamborghini Aventador",
        "attributes": [
            {
                "name": "power",
                "value": 769,
                "unit": "PS",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 350,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 12,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 800000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    },
    {
        "quiz": "car",
        "name": "McLaren 720S",
        "attributes": [
            {
                "name": "power",
                "value": 720,
                "unit": "PS",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 341,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 8,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 500000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    },
    {
        "quiz": "car",
        "name": "Porsche 911",
        "attributes": [
            {
                "name": "power",
                "value": 443,
                "unit": "PS",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 306,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 6,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 120000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    },
    {
        "quiz": "car",
        "name": "Ferrari 488 GTB",
        "attributes": [
            {
                "name": "power",
                "value": 661,
                "unit": "PS",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 329,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 8,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 204000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    },
    {
        "quiz": "car",
        "name": "Lamborghini Huracan",
        "attributes": [
            {
                "name": "power",
                "value": 602,
                "unit": "PS",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 325,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 10,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 201000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    },
    {
        "quiz": "car",
        "name": "Audi R8",
        "attributes": [
            {
                "name": "power",
                "value": 540,
                "unit": "PS",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 330,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 10,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 180000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    },
    {
        "quiz": "car",
        "name": "Aston Martin Vantage",
        "attributes": [
            {
                "name": "power",
                "value": 503,
                "unit": "PS",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 312,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 8,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 148000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    },
    {
        "quiz": "car",
        "name": "Ford Mustang GT",
        "attributes": [
            {
                "name": "power",
                "value": 460,
                "unit": "PS",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 249,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 8,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 40000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    },
    {
        "quiz": "car",
        "name": "Mercedes-AMG GT",
        "attributes": [
            {
                "name": "power",
                "value": 469,
                "unit": "PS",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 314,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 8,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 100000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    },
    {
        "quiz": "car",
        "name": "Ferrari LaFerrari",
        "attributes": [
            {
                "name": "power",
                "value": 950,
                "unit": "PS",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 350,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 12,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 2000000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    },
    {
        "quiz": "car",
        "name": "Bugatti Veyron",
        "attributes": [
            {
                "name": "power",
                "value": 1200,
                "unit": "PS",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 408,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 16,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 1900000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    },
    {
        "quiz": "car",
        "name": "Koenigsegg Agera R",
        "attributes": [
            {
                "name": "power",
                "value": 1140,
                "unit": "439",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 80,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 5,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 1500000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    },
    {
        "quiz": "car",
        "name": "Pagani Huayra",
        "attributes": [
            {
                "name": "power",
                "value": 720,
                "unit": "PS",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 370,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 12,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 2400000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    },
    {
        "quiz": "car",
        "name": "Lamborghini Veneno",
        "attributes": [
            {
                "name": "power",
                "value": 750,
                "unit": "PS",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 355,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 12,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 11000000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    },
    {
        "quiz": "car",
        "name": "Koenigseeg Jesko",
        "attributes": [
            {
                "name": "power",
                "value": 1600,
                "unit": "PS",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 480,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 5,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 3400000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    },
    {
        "quiz": "car",
        "name": "Bugatti Chiron",
        "attributes": [
            {
                "name": "power",
                "value": 1500,
                "unit": "PS",
                "maxDisplay": "is the most powerful",
                "minDisplay": "is the least powerful"
            },
            {
                "name": "speed",
                "value": 420,
                "unit": "km/h",
                "maxDisplay": "is the fastest",
                "minDisplay": "is the slowest"
            },
            {
                "name": "number of cylinders",
                "value": 8,
                "unit": "cylinder",
                "maxDisplay": "has the most cylinders",
                "minDisplay": "has the least cylinders"
            },
            {
                "name": "price",
                "value": 2900000,
                "unit": "dollars",
                "maxDisplay": "is the most expensive",
                "minDisplay": "is the cheapest"
            }
        ]
    }
]) // cars

db.objects.insertMany([
    {
        "quiz": "celebrity",
        "name": "Lewis Hamilton",
        "attributes": [
            {
                "name": "age",
                "value": 38,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 285,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 1.74,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 15,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    },
    {
        "quiz": "celebrity",
        "name": "Johnny Sins",
        "attributes": [
            {
                "name": "age",
                "value": 44,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 5,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 1.83,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 17,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    },
    {
        "quiz": "celebrity",
        "name": "Barack Obama",
        "attributes": [
            {
                "name": "age",
                "value": 61,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 48,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 1.87,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 8,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    },
    {
        "quiz": "celebrity",
        "name": "Donald Trump",
        "attributes": [
            {
                "name": "age",
                "value": 76,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 3200,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 1.90,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 4,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    },
    {
        "quiz": "celebrity",
        "name": "Jay-Z",
        "attributes": [
            {
                "name": "age",
                "value": 53,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 1300,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 1.88,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 36,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    },
    {
        "quiz": "celebrity",
        "name": "Kendrick Lamar",
        "attributes": [
            {
                "name": "age",
                "value": 35,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 75,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 1.64,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 22,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    },
    {
        "quiz": "celebrity",
        "name": "Cristiano Ronaldo",
        "attributes": [
            {
                "name": "age",
                "value": 37,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 490,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 1.87,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 21,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    },
    {
        "quiz": "celebrity",
        "name": "Lionel Messi",
        "attributes": [
            {
                "name": "age",
                "value": 35,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 600,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 1.69,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 19,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    },
    {
        "quiz": "celebrity",
        "name": "Lebron James",
        "attributes": [
            {
                "name": "age",
                "value": 38,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 1000,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 2.06,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 20,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    },
    {
        "quiz": "celebrity",
        "name": "Stephen Curry",
        "attributes": [
            {
                "name": "age",
                "value": 34,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 160,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 1.88,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 14,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    },
    {
        "quiz": "celebrity",
        "name": "Micheal Jordan",
        "attributes": [
            {
                "name": "age",
                "value": 59,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 1700,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 1.98,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 15,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    },
    {
        "quiz": "celebrity",
        "name": "Ye West",
        "attributes": [
            {
                "name": "age",
                "value": 45,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 400,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 1.72,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 27,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    },
    {
        "quiz": "celebrity",
        "name": "Ueli Maurer",
        "attributes": [
            {
                "name": "age",
                "value": 72,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 5,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 1.72,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 14,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    },
    {
        "quiz": "celebrity",
        "name": "Andrew Tate",
        "attributes": [
            {
                "name": "age",
                "value": 36,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 700,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 1.90,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 18,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    },
    {
        "quiz": "celebrity",
        "name": "Jimmy Donaldson",
        "attributes": [
            {
                "name": "age",
                "value": 24,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 100,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 1.91,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 11,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    },
    {
        "quiz": "celebrity",
        "name": "Roger Federer",
        "attributes": [
            {
                "name": "age",
                "value": 41,
                "unit": "years",
                "maxDisplay": "is the oldest",
                "minDisplay": "is the youngest"
            },
            {
                "name": "net worth",
                "value": 550,
                "unit": "million dollar",
                "maxDisplay": "is the richest",
                "minDisplay": "is the poorest"
            },
            {
                "name": "height",
                "value": 1.85,
                "unit": "meter",
                "maxDisplay": "is the tallest",
                "minDisplay": "is the shortest"
            },
            {
                "name": "active years in their area",
                "value": 25,
                "unit": "years",
                "maxDisplay": "is active the longest time",
                "minDisplay": "is active the shortest time"
            }
        ]
    }
]) // celebreties

db.objects.insertMany([
        {
            "quiz": "country",
            "name": "Switzerland",
            "attributes": [
                {
                    "name": "area",
                    "value": 41285,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 8.7,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 4,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 6.71,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1848,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "Germany",
            "attributes": [
                {
                    "name": "area",
                    "value": 375592,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 83.7,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 1,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 4.6,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1949,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "United States",
            "attributes": [
                {
                    "name": "area",
                    "value": 9833520,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 333.3,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 2,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 5.15,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1776,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "Canada",
            "attributes": [
                {
                    "name": "area",
                    "value": 9984670,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 39.3,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 2,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 5.25,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1867,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "Sri Lanka",
            "attributes": [
                {
                    "name": "area",
                    "value": 65610,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 22.2,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 2,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 3.72,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1948,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "China",
            "attributes": [
                {
                    "name": "area",
                    "value": 9597000,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 1412,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 1,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 3.56,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1949,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "India",
            "attributes": [
                {
                    "name": "area",
                    "value": 3287000,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 1408,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 2,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 2.39,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1947,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "Norwegen",
            "attributes": [
                {
                    "name": "area",
                    "value": 385207,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 5.4,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 1,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 5.25,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1867,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "Argentina",
            "attributes": [
                {
                    "name": "area",
                    "value": 2780000,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 45.8,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 1,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 4.57,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1816,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "Egypt",
            "attributes": [
                {
                    "name": "area",
                    "value": 1002000,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 109.3,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 1,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 2.43,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1953,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "Belgium",
            "attributes": [
                {
                    "name": "area",
                    "value": 30688,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 11.59,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 3,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 4.55,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1830,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "Singapore",
            "attributes": [
                {
                    "name": "area",
                    "value": 728.6,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 5.4,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 4,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 4.24,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1965,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "Venezuela",
            "attributes": [
                {
                    "name": "area",
                    "value": 916445,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 28.2,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 1,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 1.76,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1811,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "United Kingdom",
            "attributes": [
                {
                    "name": "area",
                    "value": 243610,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 67.3,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 2,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 4.44,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1922,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "Japan",
            "attributes": [
                {
                    "name": "area",
                    "value": 377973,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 125.7,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 1,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 2.83,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": -660,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "South Africa",
            "attributes": [
                {
                    "name": "area",
                    "value": 1220000,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 59.4,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 11,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 2.34,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1961,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "Colombia",
            "attributes": [
                {
                    "name": "area",
                    "value": 1099000,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 51.52,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 1,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 3.48,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1810,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "Turkey",
            "attributes": [
                {
                    "name": "area",
                    "value": 783562,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 84.78,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 1,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 2.68,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1923,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        },
        {
            "quiz": "country",
            "name": "Uruguay",
            "attributes": [
                {
                    "name": "area",
                    "value": 176215,
                    "unit": "square kilometres",
                    "maxDisplay": "is the largest",
                    "minDisplay": "is the smallest"
                },
                {
                    "name": "population",
                    "value": 84.78,
                    "unit": "million people",
                    "maxDisplay": "has the highest population",
                    "minDisplay": "has the lowest population"
                },
                {
                    "name": "languages",
                    "value": 1,
                    "unit": "offical languages",
                    "maxDisplay": "has the most offical languages",
                    "minDisplay": "has the fewest offical languages"
                },
                {
                    "name": "price of a Big-Mac",
                    "value": 6.08,
                    "unit": "U.S. dollars",
                    "maxDisplay": "has the most expensive Big-Mac",
                    "minDisplay": "has the least expensive Big-Mac"
                },
                {
                    "name": "year of foundation",
                    "value": 1825,
                    "unit": "years after christ",
                    "maxDisplay": "is the youngest",
                    "minDisplay": "is the oldest"
                }
            ]
        }


    ]) // counries