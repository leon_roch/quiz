﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using QuizBackend.Config;
using QuizBackend.Models;

namespace QuizBackend.Repositories
{
    public class QuizRepository
    {
        private readonly IMongoCollection<QuizObject> _quizCollection;
        private readonly QuizQuestionBuilder _questionBuilder;

        
        public QuizRepository(IOptions<QuizMongoDbOptions> options, QuizQuestionBuilder questionBuilder)
        {
            var mongoClient = new MongoClient(
                options.Value.ConnectionString);
            
            var mongoDatabase = mongoClient.GetDatabase(
                options.Value.DatabaseName);

            _quizCollection = mongoDatabase.GetCollection<QuizObject>(
                options.Value.QuizCollection);

            _questionBuilder = questionBuilder;
        }

        public async Task<QuizQuestion> GetQuestion(string? type)
        {
            type ??= GetRandom(GetQuizTypes().Result); // set random type if none is given
            
            var list = await _quizCollection
                .AsQueryable()
                .Where(o => o.QuizType == type)
                .Sample(3)
                .ToListAsync();

            // convert to the question object because not every attribute of the quizObject is needed
            return _questionBuilder.Build(list);

        }

        public async Task<List<string>> GetQuizTypes()
        {
            // thanks a lot chatGPT
            return await _quizCollection.Distinct<string>("QuizType", new BsonDocument()).ToListAsync();

        }

        private static T GetRandom<T>(List<T> list)
        {
            if (list is null) throw new NullReferenceException();
            
            var rand = new Random();

            var index = rand.Next(0, list.Count);

            return list[index];
        }
    }
}