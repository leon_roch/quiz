﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using QuizBackend.Config;
using QuizBackend.Models;

namespace QuizBackend.Repositories
{
    public class StatisticsRepository
    {
        private readonly IMongoCollection<Statistic> _statsCollection;

        
        public StatisticsRepository(IOptions<QuizMongoDbOptions> options)
        {
            var mongoClient = new MongoClient(
                options.Value.ConnectionString);
            
            var mongoDatabase = mongoClient.GetDatabase(
                options.Value.DatabaseName);

            _statsCollection = mongoDatabase.GetCollection<Statistic>(
                options.Value.StatisticCollection);
        }

        public async Task CreateStatistic(string user, int points, double duration)
        {
            var stat = new Statistic
            {
                User = user,
                Points = points,
                Duration = duration,
                Date = DateTime.Now.ToString("dd.MM.yyy")
            };
            
            await _statsCollection.InsertOneAsync(stat);
        }

        public async Task<List<Statistic>> GetStatisticsByUser(string user)
        {
            if (!GetUsers().Result.Contains(user)) throw new Exception($"the user {user} does not exist");

            return await _statsCollection.Aggregate()
                .Match(stat => stat.User == user)
                .SortByDescending(stat => stat.Points)
                .ThenBy(stat => stat.Duration)
                .ToListAsync();
            
        }

        private async Task<List<string>> GetUsers()
        {
            return await _statsCollection.Distinct<string>("User", new BsonDocument()).ToListAsync();
        }

        public async Task<List<Statistic>> GetTopNStatistics(int n)
        {
            if (n <= 0) throw new Exception("n has to be > 0");
            
            return await _statsCollection.Aggregate()
                .SortByDescending(stat => stat.Points)
                .ThenBy(stat => stat.Duration)
                .Limit(n)
                .ToListAsync();
        }
    }
}