﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using QuizBackend.Models;
using QuizBackend.Repositories;

namespace QuizBackend.Controllers
{
    [ApiController]
    [Route("api/quiz")]
    public class QuizController: Controller
    {
        private readonly QuizRepository _quizRepository;

        public QuizController(QuizRepository quizRepository)
        {
            _quizRepository = quizRepository;
        }

        [HttpGet("types")]
        public ActionResult<List<string>> GetQuizTypes()
        {
            return _quizRepository.GetQuizTypes().Result;
        }

        [HttpGet("question")]
        public ActionResult<QuizQuestion> GetQuestion(string? quizType)
        {
            try
            {
                return _quizRepository.GetQuestion(quizType).Result;
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}