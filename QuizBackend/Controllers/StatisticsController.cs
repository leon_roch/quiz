﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using QuizBackend.Models;
using QuizBackend.Repositories;

namespace QuizBackend.Controllers
{
    [ApiController]
    [Route("api/stats")]
    public class StatisticsController: Controller
    {
        private readonly StatisticsRepository _repository;

        public StatisticsController(StatisticsRepository repository)
        {
            _repository = repository;
        }

        [HttpPost("create")]
        public IActionResult Create( string user,  int points, double duration)
        {
            try
            {
                _repository.CreateStatistic(user, points, duration).Wait();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("user")]
        public ActionResult<List<Statistic>> GetStatsByUser(string user)
        {
            try
            {
                return _repository.GetStatisticsByUser(user).Result;
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("top/{n:int}")]
        public ActionResult<List<Statistic>> GetTopNStatistics(int n)
        {
            try
            {
                return _repository.GetTopNStatistics(n).Result;
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}